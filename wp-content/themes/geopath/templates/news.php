<?php
/**
 * Template Name: News page
 */

    get_header('page');
?>
    <section class="container-fluid intro-news-section" <?php echo renderBackgroundStyles(get_field('page_background'), get_field('page_background_color')); ?>>
        <div class="row">
            <div class="col-xs-12">
                <h1><?php the_field('page_header'); ?></h1>
                <div class="container">
                    <div class="tabs-swith-block">
                    <?php
                        $tab_1_name = get_field('news_sec_title_1');
                        $tab_2_name = get_field('news_sec_title_2');
                        $tab_3_name = get_field('news_sec_title_3');
                        $tab_4_name = get_field('news_sec_title_4');
                    ?>
                        <div class="tab-link current" data-hash="press"><?php echo $tab_1_name; ?></div>
                        <div class="tab-link" data-hash="blog"><?php echo $tab_2_name; ?></div>
                        <div class="tab-link" data-hash="industry"><?php echo $tab_3_name; ?></div>
                        <div class="tab-link" data-hash="spotlights"><?php echo $tab_4_name; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    
    <section id="press" class="tab-content container-fluid press-section" <?php echo renderBackgroundStyles(get_field('page_background'), get_field('page_background_color')); ?>>
         <div id="press-items" class="grid">
            <?php

            $press_items = new WP_Query([
                'post_type' => PRESS_POST_TYPE,
                'post_status' => 'publish',
                'cache_results'  => false,
                'orderby' => 'date',
                'order' => 'DEC',
                'posts_per_page' => PRESS_ITEMS_PER_PAGE
            ]);

            foreach ($press_items->posts as $item):
                render_press_block($item);
            endforeach;
            ?>
        </div>
        <div id="load-more" class="see-all-cont" style="<?php echo $press_items->max_num_pages > 1 ? 'display: block': 'display: none'; ?>">
            <span class="btn btn-default" data-current-page="1"><?php the_field('press_all_link_tex'); ?></span>
        </div>
    </section>
    
    <section class="container-fluid blog tab-content" <?php echo renderBackgroundStyles(get_field('page_background'), get_field('page_background_color')); ?> id="blog">
        <?php // Get RSS Feed(s)
        include_once( ABSPATH . WPINC . '/feed.php' );
        
        // Get a SimplePie feed object from the specified feed source.
        $rss = fetch_feed( get_field('rss_link') );
        $maxitems = 0;
        if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
            // Figure out how many total items there are, but limit it to 5. 
            $maxitems = $rss->get_item_quantity( -1 ); 
            // Build an array of all the items, starting with element 0 (first element).
            $rss_items = $rss->get_items( 0, $maxitems );
        endif;
        ?>
        
        <div class="blog-items-wrapper">
            <?php if ( $maxitems == 0 ) : ?>
                <li><?php _e( 'No items' ); ?></li>
            <?php else :
                 foreach ( $rss_items as $item ) :
                    $image_bg_style = false;
                    $default_img_post_url =  get_field('default_post_image');
                    
                    $default_img_post = 'background-image: url('. $default_img_post_url['url'] .')';
                    if(preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $item->get_content(), $matches)) {
                        $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $item->get_content(), $matches);
                        $first_img = $matches [1][0];
                        $image_bg_style = $first_img ? 'background-image: url('.$first_img.')' : null;
                    }
                ?>

                <div class="col-xs-12 col-sm-6 col-md-4 blog-link">
                        <div class="blog-image-container" style="<?php echo $image_bg_style ? $image_bg_style : $default_img_post; ?>">
                        </div>
                        <div class="blog-post">
                            <div class="date-post">
                                <?php 
                                    echo $item->get_date('j') . '<br>' . $item->get_date('M');
                                ?>
                            </div>
                            <h2><?php echo wp_trim_words( esc_html( $item->get_title() ) , 15, '...'); ?></h2>
                            <p><?php echo wp_trim_words($item->get_description(), 70, '...'); ?></p>

                            <?php if ($item->get_permalink()): ?>
                                <a href="<?php echo esc_url( $item->get_permalink() ) ?>" class="view-post" target="_blank">View Post</a>
                            <?php endif; ?>
                        </div>
                </div>
             <?php endforeach; ?>
                <div class="see-all-posts">
                     <a href="<?php the_field('blog_link'); ?>" target="_blank"><?php the_field('blog_link_text'); ?></a>
                </div>
            <?php endif; ?>
        </div>
    </section>
    
    <section id="industry" class="tab-content container-fluid" <?php echo renderBackgroundStyles(get_field('page_background'), get_field('page_background_color')); ?>>
        <div class="row">
            <?php if(get_field('hero_text')){ ?>
            <div class="col-xs-12">
                <p class="hero-twitter-section">
                    <?php echo the_field('hero_text'); ?>
                </p>
            </div>
            <?php 
                }
                $banner_img = get_field('banner_image');
                $style_banner = $banner_img ? 'background-image: url(' . $banner_img["url"] . ');' : 'display: none;';
            ?>
            <div class="banner-twitter" style="<?php echo $style_banner; ?>">
            </div>
        </div>
        <div class="row">
            
            <?php 
            if(get_field('twitter_user_query')) {
                $twitter_query_search = '';
                $count_name = 0;
                $count_hashtag = 0;
                foreach(get_field('twitter_user_query') as $items) {
                    if ($count_name == 0) {
                        $twitter_query_search .= 'from:' . $items['user_name'];
                    } else {
                        $twitter_query_search .= ' OR from:' . $items['user_name'];
                    }
                    $count_name++;
                }

                foreach(get_field('twitter_hashtag_query') as $items) {
                    if ($count_hashtag == 0) {
                        $twitter_query_search .= ' #' . $items['hashtag'];
                    } else {
                        $twitter_query_search .= ' OR #' . $items['hashtag'];
                    }
                    $count_hashtag++;
                }
                ?>
                <div class="twitter-wrapper" data-query="<?php echo $twitter_query_search; ?>">
                <?php get_twiter_query($twitter_query_search, 15); ?>
                </div>
                <?php
            }
            ?>
            
        </div>
    </section>
    
      <section id="spotlights" class="tab-content container-fluid spotlights-section" <?php echo renderBackgroundStyles(get_field('page_background'), get_field('page_background_color')); ?>>
         <div id="spotlights-items" class="grid">
            <?php

            $spotlights_items = new WP_Query([
                'post_type' => SPOTLIGHTS_POST_TYPE,
                'post_status' => 'publish',
                'cache_results'  => false,
                'orderby' => 'date',
                'order' => 'DEC',
                'posts_per_page' => SPOTLIGHTS_ITEMS_PER_PAGE
            ]);

            foreach ($spotlights_items->posts as $item):
                render_spotlights_block($item);
            endforeach;
            ?>
        </div>
        <div id="load-more-spotlights" class="see-all-cont" style="<?php echo $spotlights_items->max_num_pages > 1 ? 'display: block': 'display: none'; ?>">
            <span class="btn btn-default" data-current-page="1"><?php the_field('see_more_spotlights_releases_link_text'); ?></span>
        </div>
    </section>


<?php
    get_footer();
