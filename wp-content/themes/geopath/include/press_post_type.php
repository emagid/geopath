<?php

define('PRESS_POST_TYPE', 'geo_press');
define('PRESS_ITEMS_PER_PAGE', 9);
add_action('init', 'setup_press_post_type');

function setup_press_post_type()
{

    register_post_type(PRESS_POST_TYPE, [
        'label' => 'Press',
        'labels' => [
            'add_new_item' => 'Add New Press Release',
            'edit_item' => 'Edit Press Release',
            'new_item' => 'New Press Release',
            'view_item' => 'View Press Release',
            'search_items' => 'Search Press Release',
            'all_items' => 'All Press Releases',
            'not_found' => 'No Press Release found',
            'not_found_in_trash' => 'No Press Release found in Trash'
        ],
        'exclude_from_search' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'menu_positio' => 21,
        'hierarchical' => false,
        'supports' => ['title'],
    ]);
}


//Rename title placeholder
add_filter('gettext','geopath_custom_enter_press_title');

function geopath_custom_enter_press_title( $input ) {

    global $post_type;

    if( is_admin() && 'Enter title here' == $input && PRESS_POST_TYPE == $post_type )
        return 'Enter Press Release Title';

    return $input;
}



function render_press_block($item)
{
    $press_text  =  wp_trim_words(get_field('press_description', $item->ID), 40 ,'...');
    $item_title = $item->post_title;

    $press_source_link = get_field('press_source_link', $item->ID);
    $image_thumbnail = get_field('press_thumbnail', $item->ID);

    ?>
        <div class="grid-item grid-item col-sm-6 col-xs-12 col-md-4 item-container">
            <a href="<?php echo $press_source_link; ?>" target="_blank" class="library-item-link" >
                <div class="grid-item-img" <?php echo isset($image_thumbnail) ? 'style="background-image: url('. $image_thumbnail['url'] .')"' : null;?>
                >
                    <!--<img src="<?php echo $image_thumbnail['url']; ?>" alt="<?php echo $image_thumbnail['alt']; ?>" />-->
                    <div></div>
                </div>
                <div class="item-description">
                    <h3><?php echo $item_title; ?></h3>
                    <p><?php echo $press_text; ?></p>
                </div>
            </a>
        </div>
    <?php
}

/**
 * Search Items by AJAX request
 */
add_action( 'wp_ajax_search_press_items', 'search_press_items' );
add_action( 'wp_ajax_nopriv_search_press_items', 'search_press_items');

function search_press_items()
{
    $args = [
        'post_type' => PRESS_POST_TYPE,
        'post_status' => 'publish',
        'posts_per_page' => PRESS_ITEMS_PER_PAGE,
        'cache_results'  => false,
        'paged' => 1
    ];

    $query_data = [];

    if (isset($_GET['page'])) {
        $args['paged'] = intval($_GET['page']);
    }

    $query = new WP_Query($args);


    //Render html response
    $blocks = [];

    foreach ($query->posts as $post) {
        ob_start();

        render_press_block($post);
        $blocks[] = ob_get_contents();

        ob_end_clean();
    }

    //Create response json
    $response = [
        'found_posts' => intval($query->found_posts),
        'max_num_page' => $query->max_num_pages,
        'posts_per_page' => PRESS_ITEMS_PER_PAGE,
        'blocks' => $blocks
    ];

    echo json_encode($response);
    die();
}