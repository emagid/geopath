<?php 

function get_twiter_query($query, $count) {
    require_once('TwitterAPIExchange.php');
    
    $settings = array(
        'oauth_access_token' => "887638387984920576-dwk6Az5uzi3HFj5wPrY2xnUsRxkRQzV",
        'oauth_access_token_secret' => "Da3d7mipIjJD6jG1EYZYObEo5EbfxENcK05X0qhqgVPgg",
        'consumer_key' => "EkpLhzuc8bB3Mstxl0pQ49xsL",
        'consumer_secret' => "iAx9vebBqVR5UFF5bUxx14gcpg0sYO7EydzUGtaG9kXbE7QIJo",
    );
    
    $url = 'https://api.twitter.com/1.1/search/tweets.json';
    // EXAMPLE SEARCH $getfield = '?q=from:TheOBIEAwards OR from:GeopathOOH #OOH';
    $count = $count;
    
    $getfield = '?q=' . $query . '&count=' . $count;
    $requestMethod = 'GET';
    
    $twitter = new TwitterAPIExchange($settings);
    $string = json_decode($twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest(),$assoc = TRUE);
    
    if (count($string['statuses']) == 0){
          echo "<h3 class='centered'>Sorry, there was a problem.</h3><p class='centered'>No results found for your query</p>";
    } else {
    $i = 0;
    foreach($string['statuses'] as $items) { ?>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="tweet-item">
                <div class="user_block">
                    <a href="<?php 
                        $user_link = '@' . $items['user']['screen_name'];
                        $tweet_link = preg_replace("/@(\w+)/i", "http://twitter.com/$1", $user_link);
                        echo $tweet_link;
                    ?>" target="_blank">
                        <img src="<?php 
                            $profile_image_url_large = preg_replace("/_normal/", "", $items['user']['profile_image_url']);
                            echo $profile_image_url_large;
                        ?>" alt="<?php echo $items['user']['name']; ?>">
                        <p class="user-name-cont">
                            <?php echo $items['user']['name']; ?>
                            <br>
                            <span>
                                <?php echo '@' . $items['user']['screen_name']; ?>
                            </span>
                        </p>
                    </a>
                </div>
                <p><?php 
                    $tweet_text = preg_replace("/@(\w+)/i", "<a href=\"http://twitter.com/$1\" target='_blank'>$0</a>", $items['text']);
                    echo $tweet_text;
                ?></p>
                <div class="tweet_time">
                    <span class="icon-twitter"></span>
                    <?php 
                    $str_time = strtotime($items['created_at']);
                    echo date('F d, Y', $str_time); ?>
                </div>
            </div>
        </div>
        <?php
        $i++;
        } 
        
        if (($i >= $count) || ($count > 90)) {
            ?>
            <div class="load-more-tweet" data-tweet-count="<?php echo $count; ?>"><span>More Industry Tweets</span></div>
            <?php
        }
    }
    
}

/**
 * Search Items by AJAX request
 */
add_action( 'wp_ajax_get_more_tweets', 'get_more_tweets' );
add_action( 'wp_ajax_nopriv_get_more_tweets', 'get_more_tweets');

function get_more_tweets()
{
    $query_get = $_GET['query'];
    $count_get = $_GET['count'] + 15;
    
    $blocks = [];
    
    ob_start();

        get_twiter_query($query_get, $count_get);
        $blocks[] = ob_get_contents();

    ob_end_clean();
    
    echo json_encode($blocks);
    
    die();
}