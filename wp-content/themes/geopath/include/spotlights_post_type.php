<?php

define('SPOTLIGHTS_POST_TYPE', 'geo_spotlights');
define('SPOTLIGHTS_ITEMS_PER_PAGE', 9);
add_action('init', 'setup_spotlights_post_type');


function setup_spotlights_post_type()
{

    register_post_type(SPOTLIGHTS_POST_TYPE, [
        'label' => 'Spotlights',
        'labels' => [
            'add_new_item' => 'Add New Spotlights Release',
            'edit_item' => 'Edit Spotlights Release',
            'new_item' => 'New Spotlights Release',
            'view_item' => 'View Spotlights Release',
            'search_items' => 'Search Spotlights Release',
            'all_items' => 'All Spotlights Releases',
            'not_found' => 'No Spotlights Release found',
            'not_found_in_trash' => 'No Spotlights Release found in Trash'
        ],
        'exclude_from_search' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'show_in_menu' => true,
        'menu_positio' => 21,
        'hierarchical' => false,
        'supports' => ['title'],
    ]);
}


//Rename title placeholder
add_filter('gettext','geopath_custom_enter_press_title');

function geopath_custom_enter_spotlights_title( $input ) {

    global $post_type;

    if( is_admin() && 'Enter title here' == $input && PRESS_POST_TYPE == $post_type )
        return 'Enter Spotlights Release Title';

    return $input;
}



function render_spotlights_block($item)
{
    $spotlights_text  =  wp_trim_words(get_field('spotlights_description', $item->ID), 40 ,'...');
    $item_title = $item->post_title;

    $spotlights_source_link = get_field('spotlights_source_link', $item->ID);
    $image_thumbnail = get_field('spotlights_thumbnail', $item->ID);

    ?>
        <div class="grid-item grid-item col-sm-6 col-xs-12 col-md-4 item-container">
            <a href="<?php echo $spotlights_source_link; ?>" target="_blank" class="library-item-link" >
                <div class="grid-item-img" <?php echo isset($image_thumbnail) ? 'style="background-image: url('. $image_thumbnail['url'] .')"' : null;?>
                >
                    <!--<img src="<?php echo $image_thumbnail['url']; ?>" alt="<?php echo $image_thumbnail['alt']; ?>" />-->
                    <div></div>
                </div>
                <div class="item-description">
                    <h3><?php echo $item_title; ?></h3>
                    <p><?php echo $spotlights_text; ?></p>
                </div>
            </a>
        </div>
    <?php
}

/**
 * Search Items by AJAX request
 */
add_action( 'wp_ajax_search_spotlights_items', 'search_spotlights_items' );
add_action( 'wp_ajax_nopriv_search_spotlights_items', 'search_spotlights_items');

function search_spotlights_items()
{
    $args = [
        'post_type' => SPOTLIGHTS_POST_TYPE,
        'post_status' => 'publish',
        'posts_per_page' => SPOTLIGHTS_ITEMS_PER_PAGE,
        'cache_results'  => false,
        'paged' => 1
    ];

    $query_data = [];

    if (isset($_GET['page'])) {
        $args['paged'] = intval($_GET['page']);
    }

    $query = new WP_Query($args);


    //Render html response
    $blocks = [];

    foreach ($query->posts as $post) {
        ob_start();

        render_spotlights_block($post);
        $blocks[] = ob_get_contents();

        ob_end_clean();
    }

    //Create response json
    $response = [
        'found_posts' => intval($query->found_posts),
        'max_num_page' => $query->max_num_pages,
        'posts_per_page' => SPOTLIGHTS_ITEMS_PER_PAGE,
        'blocks' => $blocks
    ];

    echo json_encode($response);
    die();
}